import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Producto } from '../models/producto';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) { }

  private setHeaders(): Object {
    return {
      'headers': new HttpHeaders(
        {
          'Content-Type':'application/json'
        })
    }
  }

  public getProductos(): Observable<any> {
    return this.http.get<Producto>('http://localhost:3000/productos', this.setHeaders())
                    .pipe(
                      map(res => res)
                    );
  }

  public getProducto(id: string): Observable<any> {
    console.log(`http://localhost:3000/producto/${id}`);
    return this.http.get(`http://localhost:3000/producto/${id}`, this.setHeaders());
  }

  public saveProducto(producto: Producto) {
    const payload = {
      producto: producto
    }

    console.log(payload);
    return this.http.post(`http://localhost:3000/producto`, payload, this.setHeaders());
  }

  public deleteProducto(id: string): Observable<any> {
    console.log(`http://localhost:3000/producto/${id}`);
    return this.http.get(`http://localhost:3000/deleteProducto/${id}`, this.setHeaders());
  }

}
