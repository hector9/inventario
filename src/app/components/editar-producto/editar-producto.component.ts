import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-editar-producto',
  templateUrl: './editar-producto.component.html',
  styleUrls: ['./editar-producto.component.css']
})
export class EditarProductoComponent implements OnInit {

  producto: Producto = new Producto();
  productoId: any;
  cantidades: Array<number> = Array.from(Array(200).keys());

  constructor(
    private route: ActivatedRoute,
    private _producto: ProductoService,
    private router: Router
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('idProducto');
    if (id) {
      this._producto.getProducto(id).subscribe(result => {
        console.log(result);
        this.producto = result;
        console.log(this.producto);
      });
    }
    console.log(this.producto);
  }

  onSubmit() {
    this._producto.saveProducto(this.producto).subscribe(result => {
      console.log(result);
      this.router.navigate(['productos']);
    });
  }

}
