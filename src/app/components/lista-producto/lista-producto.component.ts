import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-lista-producto',
  templateUrl: './lista-producto.component.html',
  styleUrls: ['./lista-producto.component.css']
})
export class ListaProductoComponent implements OnInit {

  productos: Array<Producto>;

  constructor(private _producto: ProductoService) { }

  ngOnInit() {
    this._producto.getProductos().subscribe(res => {
      console.log('productos');
      console.log(res);
      this.productos = res;
    });
  }

  borrarProducto(id) {
    this._producto.deleteProducto(id).subscribe(result => {
      if (result['success']) {
        this.productos = this.productos.filter(p => p.id !== id);
      }
    });
  }

}
