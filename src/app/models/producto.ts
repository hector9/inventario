export class Producto {
    id: number;
    nombre: string;
    descripcion: string;
    cantidad: number;
    precio: number;

    constructor() {
      this.nombre = '',
      this.descripcion = '',
      this.cantidad = 0,
      this.precio = 0
    }
}
