import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaProductoComponent } from './components/lista-producto/lista-producto.component';
import { EditarProductoComponent } from './components/editar-producto/editar-producto.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'productos',
    component: ListaProductoComponent
  },
  {
    path: 'editarProducto',
    component: EditarProductoComponent
  },
  {
    path: 'editarProducto/:idProducto',
    component: EditarProductoComponent
  },
  {
    path: '404', component: NotFoundComponent
  },
  {
    path: '**', redirectTo: '/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
